#Include %A_LineFile%\..\..
#Include LibCon\LibCon.ahk

global TYPE_IDLE := "Idle"
global TYPE_BSTEP := "Bstep"
global TYPE_QUOTE := "Quote"

global currentScript := %TYPE_IDLE%
global funcRun := Func("run")
global heroesTitle := "Heroes of the Storm"

putsf("########### %s ###########", heroesTitle)
puts("Press F1 to bstep")
puts("Press F2 for a random quote")

; loading quotes from file
print("Loading quotes... ") 
Array := []
Loop
{
  FileReadLine, line, little-pigs.txt, %A_Index%
  if ErrorLevel
      break
  Array.Push(line)
}
puts("Done.")
puts("###########################################")

run() {
  global

  if WinExist(heroesTitle) {
    WinActivate, %heroesTitle%
    if (currentScript = TYPE_QUOTE) {
      quote()
    } else if (currentScript = TYPE_BSTEP) {
      bstep()
    } else if (currentScript = TYPE_IDLE) {
      puts("Idling...")
    }
  } else puts(heroesTitle . " window not found!")
}

bstep() {
  Send {b down}{b up}
  MouseClick, Right
  puts("Bstep sent.")
}

quote() {
  global
  Random, rand, Array.MinIndex(), Array.MaxIndex()
  Send {Enter} ; open chat
  Sleep, 10
  Send % Array[index]
  Sleep, 10
  Send {Enter}
}

handleTimer() {
  global
  if (currentScript = TYPE_QUOTE) 
    SetTimer, %funcRun%, 5000
  else if (currentScript = TYPE_BSTEP) 
    SetTimer, %funcRun%, 200
  else if (currentScript = TYPE_IDLE) 
    SetTimer, %funcRun%, Off
}

changeScript(newType) {
  if (newType != currentScript) {
    currentScript := newType
    if (newType == TYPE_BSTEP)
        Send {Space down}
    else Send {Space up}
  } else currentScript := TYPE_IDLE
  
  puts("Switched to: " . currentScript)
  if (currentScript != TYPE_IDLE)
    run() ; run once before the timer delay

  handleTimer()
}

F1::
  changeScript(TYPE_BSTEP)
Return

F2::
  changeScript(TYPE_QUOTE)
Return