#Include %A_LineFile%\..\..
#Include LibCon\LibCon.ahk

global timerActive := False
global funcReplay := Func("replay")
global raidTitle := "Raid: Shadow Legends"
global DELAY_MS_REPLAY := 10000
global times := 0

; create a timer and turn it off by default
SetTimer, %funcReplay%, %DELAY_MS_REPLAY%
SetTimer, %funcReplay%, Off

putsf("########## %s ###########", raidTitle)
puts("Press F3 to start replay timer")
putsf("Timer currently set to: %s seconds", Floor(DELAY_MS_REPLAY / 1000))
puts("###########################################")

return

replay() {
  global

  putsf("[%s%] script progress.", Round(times / timesMax * 100, 2))
  if(times == timesMax)
    onF3()

  if WinExist(raidTitle) {
    puts("Changing to: " . raidTitle)

    WinGetActiveTitle, currentWindowTitle
    WinActivate, %raidTitle%
    Sleep, 100
    Send {r down} {r up}
    Sleep, 100
    puts("Replay command sent.")
    WinActivate, %currentWindowTitle%
  } else puts(raidTitle . " not found!")

  times := times + 1
}

onF3() {
    global timerActive := !timerActive
  ; handle timer according to timerActive status
  if(timerActive) {
    SetFgColor(Green)
    print("For how many minutes should the script run? ")
    SetFgColor(White)
    
    replayTime = ; clear the variable so gets works
    gets(replayTime)

    global timesMax := replayTime * 60 * 1000 / DELAY_MS_REPLAY
    global times := 0
    puts("Script activated.")
    replay()
    SetTimer, %funcReplay%, On
  } else {
    puts("Script deactivated.")
    SetTimer, %funcReplay%, Off
  }
}

F3::
  onF3()
Return

