; set default include path to lib folder
#Include %A_LineFile%\..\lib
#Include LibCon\LibCon.ahk

#SingleInstance force ; closes a currently running instant and replaces it
#Persistent ; keeps the script active until the user closes it
SendMode, Input

SmartStartConsole() ; inits and shows the console

SetConsoleWidth(80)
SetConsoleHeight(50)

start:
puts("######### G A M E - S C R I P T S #########")
puts("TIP: CTRL + C closes the console / script")
puts("###########################################")
newline()
puts("1) Heroes of the Storm")
puts("2) Raid: Shadow Legends")
newline()
SetFgColor(Green)
print("Choose a game by entering the index: ")
SetFgColor(White)
gets(input)

cls()
if (input == "1") {
  #Include LibGames\HeroesOfTheStorm.ahk
} else if (input == "2") {
  #Include LibGames\RaidShadowLegends.ahk
} else {
  SetFgColor(Red)
  puts("INVALID INPUT!")
  SetFgColor(White)
  Goto, start
}

